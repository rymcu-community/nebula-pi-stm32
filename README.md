<p align="center">
<img alt="logo" src="https://rymcu.com/_nuxt/img/rymcu.3b63a55.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">nebula-pi-stm32</h1>
<h4 align="center">零基础入门STM32，基于HAL开发！</h4>
<p align="center">
    <a href="https://gitee.com/rymcu-community/nebula-pi-stm32/stargazers"><img src="https://gitee.com/rymcu-community/nebula-pi-stm32/badge/star.svg?theme=starts"></a>
    <a href="https://gitee.com/hugh-rymcu"><img src="https://img.shields.io/badge/石头人-v20-brightgreen.svg"></a>
    <a href="https://gitee.com/rymcu-community"><img src="https://img.shields.io/badge/rymcu-community-brightgreen.svg"></a>
    <a href="https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

# 零基础入门STM32，基于HAL开发！  

#### [1.点亮你的第一个LED灯(LED)](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/1-LED/1.%E7%82%B9%E4%BA%AE%E4%BD%A0%E7%9A%84%E7%AC%AC%E4%B8%80%E4%B8%AALED.md)

#### [2.蜂鸣器”滴滴答答“(Buzzer)](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/2-Buzzer/2.%E8%9C%82%E9%B8%A3%E5%99%A8.md)

#### [3.按键输入(KEY)](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/3-KEY/3.%E6%8C%89%E9%94%AE.md)

#### [4.串口通信(USART)](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/4-USART/4.%E4%B8%B2%E5%8F%A3%E9%80%9A%E4%BF%A1.md)

#### [5.外部中断(EXTI)](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/5-EXTI/5.%E5%A4%96%E9%83%A8%E4%B8%AD%E6%96%AD.md)

#### [6.IIC通信](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/6-IIC/6.IIC.md)

#### [7.SPI通信](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/7-SPI/7.SPI.md)

#### [8.FATFS文件系统](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/8-FATFS/8.FATFS.md)

#### [9.OLED显示](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/9-OLED/9.OLED.md)

#### [10.CAN通信](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/10-CAN/10.CAN.md)

#### [11.读写内部FLASH](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/11-%E8%AF%BB%E5%86%99%E5%86%85%E9%83%A8FLASH/11.%E8%AF%BB%E5%86%99%E5%86%85%E9%83%A8FLASH.md)

#### [12.读取UID和FLASH容量](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/12-%E8%AF%BB%E5%8F%96UID%E5%92%8CFLASH%E5%AE%B9%E9%87%8F/12.%E8%AF%BB%E5%8F%96UID%E5%92%8CFLASH%E5%AE%B9%E9%87%8F.md)

#### [13.单片机内部温度传感器](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/13-%E5%8D%95%E7%89%87%E6%9C%BA%E5%86%85%E9%83%A8%E6%B8%A9%E5%BA%A6%E4%BC%A0%E6%84%9F%E5%99%A8/13.%E5%8D%95%E7%89%87%E6%9C%BA%E5%86%85%E9%83%A8%E6%B8%A9%E5%BA%A6%E4%BC%A0%E6%84%9F%E5%99%A8.md)

#### [14.USB虚拟串口](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/14-USB%E8%99%9A%E6%8B%9F%E4%B8%B2%E5%8F%A3/14.USB%E8%99%9A%E6%8B%9F%E4%B8%B2%E5%8F%A3.md)

#### [15.HAL库USB HID键盘设备](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/15-USB_HID/15.USBHID.md)

#### [16.SPI_FLASH模拟U盘FATFS](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/15-USB_HID/15.USBHID.md)

#### [17.串口DMA传输](https://gitee.com/rymcu-community/nebula-pi-stm32/blob/master/17-%E4%B8%B2%E5%8F%A3DMA%E4%BC%A0%E8%BE%93/17.%E4%B8%B2%E5%8F%A3DMA%E9%80%9A%E4%BF%A1.md)



#### 持续更新....
